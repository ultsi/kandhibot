/*
  Kandhibot, a markov chain chatbot for Telegram
  Copyright (C) 2019 Joonas Ulmanen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import TelegramBot, { Message } from "node-telegram-bot-api";
import { MarkovTextGenerator } from "./markov";
import { readFileSync, readFile } from "fs";

const TOKEN = process.env.TOKEN || "not set";
process.env.TOKEN = "***SECRET***";

if (TOKEN === "not set") {
  throw new Error("TOKEN is not set!");
}

const MARKOV_ORDER = 2;
const MARKOV_DELIMITER = " ";
const MARKOV_RESPONSE_LIMIT = 150;
const BOT_NAME = "Kandhibot";

const bot = new TelegramBot(TOKEN, { polling: true });
const generator = new MarkovTextGenerator(MARKOV_ORDER, MARKOV_DELIMITER);
const files = ["joonas.txt", "leila.txt", "joel.txt", "jose.txt"];
const filedata = files.map((filename: string) =>
  readFileSync(`data/${filename}`)
    .toString()
    .toLowerCase()
    .replace(/\s+/g, " ")
);

generator.seed(filedata);

bot.on("message", async (msg: Message) => {
  if (
    !(msg.text || msg.caption) ||
    msg.chat.type === "channel" ||
    !msg.from ||
    msg.from.is_bot ||
    msg.forward_from
  ) {
    return;
  }

  const chatId = msg.chat.id;

  const text = msg.text || msg.caption || "";
  // remove Mentions
  const wordsWithoutMentions = stripMentionsFromWords(
    text.split(MARKOV_DELIMITER)
  );
  const wholeTextLowerCase = wordsWithoutMentions
    .join(MARKOV_DELIMITER)
    .toLowerCase();
  const mention =
    wordsWithoutMentions[0].toLowerCase() === `${BOT_NAME.toLowerCase()}`;
  const searchText = mention ? `@${wholeTextLowerCase}` : wholeTextLowerCase;
  const generatedResponseWords = generator.generateFromData(
    searchText,
    MARKOV_RESPONSE_LIMIT
  );
  const response = autoCapitalizeWords(generatedResponseWords).join(
    MARKOV_DELIMITER
  );
  await bot.sendMessage(chatId, response);
});

function matchLastCharacter(str: string, to: RegExp) {
  return str && to.test(str[str.length - 1]);
}

function autoCapitalizeWords(words: string[]): string[] {
  return words.map((word: string, index: number) =>
    index === 0
      ? capitalizeFirstLetter(word)
      : matchLastCharacter(words[index - 1], /[\.\?\!]/)
      ? capitalizeFirstLetter(word)
      : word
  );

  function capitalizeFirstLetter(text: string) {
    return text[0].toUpperCase() + text.substring(1);
  }
}

function stripMentionsFromWords(words: string[]): string[] {
  return words.map(word => (word[0] === "@" ? word.substring(1) : word));
}
